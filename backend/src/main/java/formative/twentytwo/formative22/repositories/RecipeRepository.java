package formative.twentytwo.formative22.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.twentytwo.formative22.models.Recipe;




public interface RecipeRepository extends CrudRepository<Recipe, Integer> {

    @Query(value ="SELECT * FROM recipe", nativeQuery = true)
    List<Recipe> findAll();

    @Query(value ="SELECT * FROM recipe where UPPER(title) LIKE %?1% OR LOWER(description) LIKE %?1%", nativeQuery = true)
    List<Recipe> search(String search);
    

}
