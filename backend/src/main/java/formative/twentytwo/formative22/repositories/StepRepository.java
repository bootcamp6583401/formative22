package formative.twentytwo.formative22.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.twentytwo.formative22.models.Step;




public interface StepRepository extends CrudRepository<Step, Integer> {

    @Query(value ="SELECT * FROM step", nativeQuery = true)
    List<Step> findAll();
}
