package formative.twentytwo.formative22.repositories;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import formative.twentytwo.formative22.models.Ingredient;




public interface IngredientRepository extends CrudRepository<Ingredient, Integer> {

    @Query(value ="SELECT * FROM ingredient", nativeQuery = true)
    List<Ingredient> findAll();
}
