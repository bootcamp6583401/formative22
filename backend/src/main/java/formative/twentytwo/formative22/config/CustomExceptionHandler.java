// package formative.twentytwo.formative22.config;

// import java.util.ArrayList;
// import java.util.HashMap;
// import java.util.List;
// import java.util.Map;

// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.web.bind.annotation.ControllerAdvice;
// import org.springframework.web.bind.annotation.ExceptionHandler;

// import jakarta.validation.ConstraintViolation;
// import jakarta.validation.ConstraintViolationException;

// @ControllerAdvice
// public class CustomExceptionHandler {
//     @ExceptionHandler(ConstraintViolationException.class)
//     public ResponseEntity<Object> handleException(ConstraintViolationException ex) {
//         List<String> messages = new ArrayList<>();
//         List<ConstraintViolation<?>> vios =  new ArrayList<>(ex.getConstraintViolations());

//         for (ConstraintViolation<?> constraintViolation : vios) {
//             messages.add(  constraintViolation.getMessage());
//         }
        

//         Map<String, Object> body = new HashMap<>();
//         body.put("message", messages);
        

//         return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
//     }
// }