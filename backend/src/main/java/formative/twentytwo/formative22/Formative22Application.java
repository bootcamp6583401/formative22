package formative.twentytwo.formative22;

import org.springframework.aot.generate.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Generated
public class Formative22Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative22Application.class, args);
	}

}
