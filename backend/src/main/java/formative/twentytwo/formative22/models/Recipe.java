package formative.twentytwo.formative22.models;

import java.util.List;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    @NotNull(message = "Name cannot be empty")
    private String title;

    @Getter
    @Setter
    private String description;


    

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "recipe", cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonManagedReference(value ="steps")
    @Getter
    @Setter
    private List<Step> steps;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "recipe", cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonManagedReference(value ="ingredients")
    @Getter
    @Setter
    private List<Ingredient> ingredients;

    // public String getDepartmentName(){
    //     if(this.getDepartment() == null) return "Unemployed";
    //     return this.getDepartment().getName();
    // }


}

