package formative.twentytwo.formative22.models;


import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
public class Ingredient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private Integer id;

    @Getter
    @Setter
    @NotNull(message = "Name cannot be empty")
    private String name;

    @Getter
    @Setter
    private int qty;


    @ManyToOne
    @JoinColumn(name = "recipe_id", referencedColumnName = "id")
    @JsonBackReference(value ="ingredients")
    @Getter
    @Setter
    private Recipe recipe;
}
