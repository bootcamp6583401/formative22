package formative.twentytwo.formative22.services.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import formative.twentytwo.formative22.models.Recipe;
import formative.twentytwo.formative22.repositories.IngredientRepository;
import formative.twentytwo.formative22.repositories.RecipeRepository;
import formative.twentytwo.formative22.repositories.StepRepository;
import formative.twentytwo.formative22.services.RecipeService;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    RecipeRepository repo;

    @Autowired
    StepRepository stepRepo;

    @Autowired 
    IngredientRepository ingredientRepo;

    public List<Recipe> getAll() {
        return repo.findAll();
    }

    public List<Recipe> search(String search) {
        return repo.search(search.toLowerCase());
    }

    public Recipe save(Recipe recipe) {
        Recipe savedRecipe = repo.save(recipe);

        ingredientRepo.saveAll(recipe.getIngredients());

        stepRepo.saveAll(recipe.getSteps());

        return savedRecipe;
    }

    public Recipe getById(int id) {
        Optional<Recipe> res = repo.findById(id);
        if (res.isPresent()) {
            return res.get();
        }
        return null;
    }

    public Recipe updateById(int id, Recipe recipe) {

        recipe.setId(id);
        Recipe savedRecipe = repo.save(recipe);

        ingredientRepo.saveAll(recipe.getIngredients());

        stepRepo.saveAll(recipe.getSteps());

        return savedRecipe;

    }

    public void deleteById(int id) {
        repo.deleteById(id);
    }


}