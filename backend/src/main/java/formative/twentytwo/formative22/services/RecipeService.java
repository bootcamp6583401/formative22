package formative.twentytwo.formative22.services;


import java.util.List;

import formative.twentytwo.formative22.models.Recipe;




public interface RecipeService {
    List<Recipe> getAll();

    Recipe getById(int id);

    Recipe save(Recipe obj);

    void deleteById(int id);

    Recipe updateById(int id, Recipe brand);

    List<Recipe> search(String search);


}
