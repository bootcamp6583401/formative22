package formative.twentytwo.formative22.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import formative.twentytwo.formative22.models.Recipe;
import formative.twentytwo.formative22.services.RecipeService;



@RestController
@RequestMapping("/recipes")
@Validated
public class RecipeController {

    @Autowired
    RecipeService service;


    @Autowired
    RecipeService depService;

    @GetMapping
    public Iterable<Recipe> getAll(@RequestParam(required =  false) String search) {
        if(search != null) {
            return service.search(search);
        }
        return service.getAll();
    }

    @PostMapping
    public Recipe save(@RequestBody Recipe recipe) {
       
        return service.save(recipe);
    }


    @GetMapping("/{id}")
    public Recipe getById(@PathVariable int id) {
        return service.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable int id) {
        service.deleteById(id);
    }

    @PutMapping("/{id}")
    public Recipe updateById(@PathVariable int id, @RequestBody Recipe recipe) {
        return service.updateById(id, recipe);
    }

}