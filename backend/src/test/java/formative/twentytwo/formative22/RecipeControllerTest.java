package formative.twentytwo.formative22;




import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import formative.twentytwo.formative22.controllers.RecipeController;
import formative.twentytwo.formative22.models.Ingredient;
import formative.twentytwo.formative22.models.Recipe;
import formative.twentytwo.formative22.models.Step;
import formative.twentytwo.formative22.services.RecipeService;

import java.util.List;
import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RecipeController.class)
public class RecipeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecipeService service;

    @Test
    public void getAllRecipes_thenReturnJsonArray() throws Exception {
        Recipe recipe1 = new Recipe();
        recipe1.setId(1);
        recipe1.setTitle("Recipe1");
        recipe1.setDescription("Desc1");

        Recipe recipe2 = new Recipe();
        recipe2.setId(2);
        recipe2.setTitle("Recipe2");
        recipe2.setDescription("Desc2");


        Step step1 = new Step();
        step1.setId(1);
        step1.setName("Name");
        step1.setRecipe(recipe1);

        recipe1.setSteps(Arrays.asList(step1));

        Ingredient ing1 = new Ingredient();
        ing1.setId(1);
        ing1.setName("Name");
        ing1.setQty(1);
        ing1.setRecipe(recipe1);

        recipe1.setIngredients(Arrays.asList(ing1));


        List<Recipe> allRecipes = Arrays.asList(recipe1, recipe2);

        given(service.getAll()).willReturn(allRecipes);

        assertEquals(ing1.getRecipe(), step1.getRecipe());

        mockMvc.perform(get("/recipes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(recipe1.getId())))
                .andExpect(jsonPath("$[0].title", is(recipe1.getTitle())))
                .andExpect(jsonPath("$[0].description", is(recipe1.getDescription())))
                .andExpect(jsonPath("$[0].steps[0].name", is(step1.getName())))
                .andExpect(jsonPath("$[0].ingredients[0].name", is(ing1.getName())))
                .andExpect(jsonPath("$[0].ingredients[0].qty", is(ing1.getQty())))
                .andExpect(jsonPath("$[1].title", is(recipe2.getTitle())));
    }

    @Test
    public void getRecipeById_WhenRecipeExists() throws Exception {
        int recipeId = 1;
        Recipe recipe = new Recipe();
        recipe.setId(recipeId);
        recipe.setTitle("Recipe1");

        given(service.getById(recipeId)).willReturn(recipe);

        mockMvc.perform(get("/recipes/{id}", recipeId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(recipe.getTitle())));
    }

    @Test
    public void createRecipe_WhenPostRecipe() throws Exception {
        Recipe recipe = new Recipe();
        recipe.setId(1);
        recipe.setTitle("Recipe1");

        given(service.save(any(Recipe.class))).willReturn(recipe);

        mockMvc.perform(post("/recipes")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"title\":\"Recipe1\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(recipe.getTitle())));
    }

    

    @Test
    public void deleteRecipe_WhenRecipeExists() throws Exception {
        int recipeId = 1;

        doNothing().when(service).deleteById(recipeId);

        mockMvc.perform(delete("/recipes/{id}", recipeId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateRecipe_WhenPutRecipe() throws Exception {
        int recipeId = 1;
        Recipe recipe = new Recipe();
        recipe.setId(recipeId);
        recipe.setTitle("Updated Recipe");

        given(service.updateById(eq(recipeId), any(Recipe.class))).willReturn(recipe);

        mockMvc.perform(put("/recipes/{id}", recipeId)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Updated Recipe\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(recipe.getTitle())));
    }
}