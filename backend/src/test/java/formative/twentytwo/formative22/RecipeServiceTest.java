package formative.twentytwo.formative22;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import formative.twentytwo.formative22.models.Recipe;
import formative.twentytwo.formative22.repositories.IngredientRepository;
import formative.twentytwo.formative22.repositories.RecipeRepository;
import formative.twentytwo.formative22.repositories.StepRepository;
import formative.twentytwo.formative22.services.impl.RecipeServiceImpl;




@SpringBootTest
public class RecipeServiceTest {
    @Mock
    private RecipeRepository repo;

    @Mock
    private IngredientRepository ingredientRepo;

    @Mock
    private StepRepository stepRepo;

    @InjectMocks
    private RecipeServiceImpl service;

    @Test
    public void whenGetAllRecipes_thenReturnRecipeList() {
        Recipe obj = new Recipe();
        obj.setId(1);
        obj.setTitle("Test Recipe");

        when(repo.findAll()).thenReturn(Collections.singletonList(obj));

        List<Recipe> result = service.getAll();

        assertEquals(1, result.size());
        assertEquals("Test Recipe", result.get(0).getTitle());
    }

    @Test
    public void whenGetRecipeById_thenReturnRecipe() {
        Recipe obj = new Recipe();
        obj.setId(1);
        obj.setTitle("Test Recipe");

        when(repo.findById(1)).thenReturn(Optional.of(obj));

        Recipe result = service.getById(1);

        assertEquals("Test Recipe", result.getTitle());
    }

    @Test
    public void whenNotFound() {

        when(repo.findById(1)).thenReturn(Optional.ofNullable(null));
        Recipe result = service.getById(1);
        assertNull(result);
    }

    @Test
    public void whenSaveRecipe_thenRecipeShouldBeSaved() {
        Recipe obj = new Recipe();
        obj.setId(1);
        obj.setTitle("Test Recipe");

        obj.setIngredients(List.of());
        obj.setSteps(List.of());

        when(repo.save(any(Recipe.class))).thenReturn(obj);

        Recipe savedRecipe = service.save(obj);
        

        assertNotNull(savedRecipe);
        assertEquals(obj.getTitle(), savedRecipe.getTitle());
        verify(repo, times(1)).save(obj);
    }


    @Test
    public void whenDeleteRecipe_thenRecipeShouldBeDeleted() {
        int objId = 1;

        doNothing().when(repo).deleteById(objId);
        service.deleteById(objId);

        verify(repo, times(1)).deleteById(objId);
    }

    @Test
    public void whenUpdateRecipe_thenRecipeShouldBeUpdated() {
        Recipe existingRecipe = new Recipe();
        existingRecipe.setId(1);
        existingRecipe.setTitle("Old Recipe Name");

        Recipe updated = new Recipe();
        updated.setId(1);
        updated.setTitle("Updated Recipe Name");

        when(repo.findById(existingRecipe.getId())).thenReturn(Optional.of(existingRecipe));
        when(repo.save(any(Recipe.class))).thenAnswer(i -> i.getArguments()[0]);

        Recipe updatedRecipe = service.updateById(existingRecipe.getId(), updated);

        assertNotNull(updatedRecipe);
        assertEquals(updated.getTitle(), updatedRecipe.getTitle());
        verify(repo, times(1)).save(updatedRecipe);
    }

}

