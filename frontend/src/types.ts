export type Ingredient = {
  id: number;
  createId?: string | number;
  name: string;
  qty: number;
};

export type Step = {
  id: number;
  createId?: string | number;
  name: string;
};

export type Recipe = {
  id: number;
  title: string;
  description: string;
  steps: Step[];
  ingredients: Ingredient[];
};

export enum Page {
  HOME = "HOME",
  FORM = "FORM",
  FAVORITE = "FAVORITE",
  DETAIL = "DETAIL",
}
