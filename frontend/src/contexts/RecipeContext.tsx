import axios from "@/lib/axios";
import { Page, Recipe } from "@/types";
import React, {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";
import { useLocalStorage } from "usehooks-ts";

export interface RecipeContextData {
  recipes: Recipe[];
  loading: boolean;
  setRecipes: React.Dispatch<React.SetStateAction<Recipe[]>>;
  page: Page;
  setPage: React.Dispatch<React.SetStateAction<Page>>;
  selectedRecipe: null | Recipe;
  setSelectedRecipe: React.Dispatch<React.SetStateAction<Recipe | null>>;
  setFavorites: React.Dispatch<React.SetStateAction<number[]>>;
  favorites: number[];
  favoriteMode: boolean;
  setFavoriteMode: React.Dispatch<React.SetStateAction<boolean>>;
}

export const UserContext = createContext<RecipeContextData>({
  recipes: [],
  loading: true,
  setRecipes: () => {},
  page: Page.HOME,
  setPage: () => {},
  selectedRecipe: null,
  setSelectedRecipe: () => {},
  setFavorites: () => {},
  favorites: [],
  setFavoriteMode: () => {},
  favoriteMode: false,
});

export const RecipeContextProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [recipes, setRecipes] = useState<Recipe[]>([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState<Page>(Page.HOME);
  const [selectedRecipe, setSelectedRecipe] = useState<Recipe | null>(null);
  const [favorites, setFavorites] = useLocalStorage<number[]>("favorites", []);
  const [favoriteMode, setFavoriteMode] = useState(false);

  useEffect(() => {
    (async () => {
      const res = await axios.get("/recipes");
      setRecipes(res.data as Recipe[]);
      setLoading(false);
    })();
  }, []);

  return (
    <UserContext.Provider
      value={{
        page,
        setPage,
        recipes,
        loading,
        setRecipes,
        selectedRecipe,
        setSelectedRecipe,
        setFavorites,
        favorites,
        setFavoriteMode,
        favoriteMode,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export const useRecipes = (): RecipeContextData => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error("useRecipes must be used within a RecipeContextProvider");
  }
  return context;
};
