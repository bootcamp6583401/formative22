/* v8 ignore next 200 */

import { Step } from "@/types";
import { Input } from "@/components/ui/input";
import { Trash2 } from "lucide-react";
import { Button } from "./ui/button";

export default function StepRow({
  step,
  setSteps,
}: {
  step: Omit<Step, "id">;
  setSteps: React.Dispatch<React.SetStateAction<Omit<Step, "id">[]>>;
}) {
  return (
    <div className="flex items-center gap-4">
      <Input
        placeholder="Description of your recipe"
        value={step.name}
        className="block"
        onChange={(e) => {
          setSteps((prev) =>
            prev.map((st) => {
              if (st.createId === step.createId) {
                return { ...st, name: e.target.value };
              } else {
                return st;
              }
            })
          );
        }}
      />
      <Button
        className="ml-auto block"
        variant="destructive"
        onClick={() =>
          setSteps((prev) => prev.filter((st) => st.createId !== step.createId))
        }
      >
        <Trash2 />
      </Button>
    </div>
  );
}
