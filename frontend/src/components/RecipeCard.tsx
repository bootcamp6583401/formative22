/* v8 ignore next 200 */

import { useRecipes } from "@/contexts/RecipeContext";
import { Recipe } from "@/types";
import { Star } from "lucide-react";

export default function RecipeCard({ recipe }: { recipe: Recipe }) {
  const { setFavorites, favorites } = useRecipes();
  return (
    <div className="rounded-md overflow-hidden shadow-md w-full max-w-full">
      <img
        className="w-full object-cover h-40"
        src="https://d1vbn70lmn1nqe.cloudfront.net/prod/wp-content/uploads/2023/07/31040437/ini-resep-bumbu-rendang-daging-sapi-untuk-acara-spesial.jpg"
      />
      <div className="p-4">
        <div className="flex justify-between items-center">
          <div className="text-xl font-bold">{recipe.title}</div>
          <button
            onClick={(e) => {
              e.stopPropagation();
              if (favorites.includes(recipe.id)) {
                setFavorites((prev) => prev.filter((fav) => fav !== recipe.id));
              } else {
                setFavorites((prev) =>
                  Array.from(new Set([...prev, recipe.id]))
                );
              }
            }}
          >
            <Star fill={favorites.includes(recipe.id) ? "#16A34A" : "white"} />
          </button>
        </div>
        <div className="tracking-tight">{recipe.description}</div>
      </div>
    </div>
  );
}
