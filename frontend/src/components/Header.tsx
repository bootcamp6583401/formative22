import { useDebounceValue } from "usehooks-ts";
import { Input } from "./ui/input";
import { useEffect, useState } from "react";
import { useRecipes } from "@/contexts/RecipeContext";
import axios from "@/lib/axios";
import { Recipe } from "@/types";

export default function Header() {
  const { setRecipes, setFavoriteMode, favoriteMode } = useRecipes();
  const [debouncedSearch, setSearch] = useDebounceValue("", 500);
  const [searchVal, setSearchVal] = useState("");
  useEffect(() => {
    (async () => {
      const res = await axios.get(`/recipes?search=${debouncedSearch}`);
      setRecipes(res.data as Recipe[]);
    })();
  }, [debouncedSearch]);

  return (
    <header className="p-4 bg-green-600">
      <nav className="flex justify-between items-center">
        <div className="text-xl font-bold text-white">Max Recipe</div>
        <div className="flex items-center gap-8">
          <button
            className="text-white font-semibold"
            onClick={() => {
              setFavoriteMode((prev) => !prev);
            }}
          >
            {favoriteMode ? "All" : "Favorites"}
          </button>
          <Input
            placeholder="Search recipes..."
            value={searchVal}
            onChange={(e) => {
              setSearch(e.target.value);
              setSearchVal(e.target.value);
            }}
          />
        </div>
      </nav>
    </header>
  );
}
