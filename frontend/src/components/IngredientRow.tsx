/* v8 ignore next 200 */

import { Ingredient } from "@/types";
import { Input } from "@/components/ui/input";
import { Trash2 } from "lucide-react";
import { Button } from "./ui/button";

export default function IngredientRow({
  ingredient,
  setIngredients,
}: {
  ingredient: Omit<Ingredient, "id">;
  setIngredients: React.Dispatch<
    React.SetStateAction<Omit<Ingredient, "id">[]>
  >;
}) {
  return (
    <div className="flex items-center gap-4">
      <Input
        placeholder="Name of ingredient"
        value={ingredient.name}
        className="block"
        onChange={(e) => {
          setIngredients((prev) =>
            prev.map((ing) => {
              if (ing.createId === ingredient.createId) {
                return { ...ing, name: e.target.value };
              } else {
                return ing;
              }
            })
          );
        }}
      />
      <Input
        placeholder="Qty"
        value={ingredient.qty}
        type="number"
        className="block w-16"
        onChange={(e) => {
          setIngredients((prev) =>
            prev.map((ing) => {
              if (ing.createId === ingredient.createId) {
                return { ...ing, qty: parseInt(e.target.value) };
              } else {
                return ing;
              }
            })
          );
        }}
      />
      <Button
        className="ml-auto block"
        variant="destructive"
        onClick={() =>
          setIngredients((prev) =>
            prev.filter((st) => st.createId !== ingredient.createId)
          )
        }
      >
        <Trash2 />
      </Button>
    </div>
  );
}
