import "./App.css";
import Header from "./components/Header";
import Home from "./pages/Home";
import Form from "./pages/Form";
import { useRecipes } from "./contexts/RecipeContext";
import { Page } from "./types";
import Details from "./pages/Details";

function App() {
  const { page, selectedRecipe } = useRecipes();
  return (
    <div>
      <Header />
      <main className="mx-auto container px-8 mt-8">
        {page === Page.HOME && <Home />}
        {page === Page.FORM && <Form />}
        {page === Page.DETAIL && selectedRecipe && (
          <Details recipe={selectedRecipe} />
        )}
      </main>
    </div>
  );
}

export default App;
