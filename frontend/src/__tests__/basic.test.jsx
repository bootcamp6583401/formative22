import renderer from "react-test-renderer";
import { test, expect, vi } from "vitest";
import App from "../App";
import { RecipeContextProvider } from "../contexts/RecipeContext";
import IngredientRow from "../components/IngredientRow";
import StepRow from "../components/StepRow";
import RecipeCard from "../components/RecipeCard";
import Form from "../pages/Form";

function toJson(component) {
  const result = component.toJSON();
  expect(result).toBeDefined();
  expect(result).not.toBeInstanceOf(Array);
  return result;
}

test("App", () => {
  const component = renderer.create(<App />);
  let tree = toJson(component);
  expect(tree).toMatchSnapshot();
});

global.fetch = vi.fn();

function createFetchResponse(data) {
  return { json: () => new Promise((resolve) => resolve(data)) };
}

test("ContextProvider", () => {
  const res = [
    {
      id: 1,
      title: "name",
      description: "test",
      ingredients: [{ id: 1, name: "sdf", qty: 1 }],
      steps: [{ id: 1, name: "sdf" }],
    },
  ];

  fetch.mockResolvedValue(createFetchResponse(res));

  const component = renderer.create(<RecipeContextProvider />);
  let tree = toJson(component);
  expect(tree).toMatchSnapshot();
});
