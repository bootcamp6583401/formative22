/* v8 ignore next 200 */

import IngredientRow from "@/components/IngredientRow";
import StepRow from "@/components/StepRow";
import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import { useRecipes } from "@/contexts/RecipeContext";
import axios from "@/lib/axios";
import { Ingredient, Page, Recipe, Step } from "@/types";
import { Plus } from "lucide-react";
import { useEffect, useState } from "react";

import { v4 as uuidv4 } from "uuid";

export default function () {
  const { setPage, setRecipes, selectedRecipe, setSelectedRecipe } =
    useRecipes();
  const [steps, setSteps] = useState<Omit<Step, "id">[]>([
    {
      createId: uuidv4(),
      name: "",
    },
  ]);
  const [ingredients, setIngredients] = useState<Omit<Ingredient, "id">[]>([
    {
      createId: uuidv4(),
      name: "",
      qty: 0,
    },
  ]);

  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  const handleAddStepRow = () => {
    setSteps((prev) => [...prev, { createId: uuidv4(), name: "" }]);
  };

  const handleAddIngredientRow = () => {
    setIngredients((prev) => [
      ...prev,
      { createId: uuidv4(), name: "", qty: 0 },
    ]);
  };

  const handleSubmit = async () => {
    if (!selectedRecipe) {
      const res = await axios.post("/recipes", {
        title,
        description,
        steps: steps.map((st) => ({ name: st.name })),
        ingredients: ingredients.map((st) => ({ name: st.name, qty: st.qty })),
      });
      setRecipes((prv) => [...prv, res.data as Recipe]);
      setPage(Page.HOME);
    } else {
      const res = await axios.put(`/recipes/${selectedRecipe.id}`, {
        title,
        description,
        steps: steps.map((st) => ({
          name: st.name,
          id: typeof st.createId === "number" ? st.createId : undefined,
        })),
        ingredients: ingredients.map((st) => ({
          name: st.name,
          qty: st.qty,
          id: typeof st.createId === "number" ? st.createId : undefined,
        })),
      });
      const updated = res.data as Recipe;
      setRecipes((prv) =>
        prv.map((recipe) => (recipe.id === updated.id ? updated : recipe))
      );
      setSelectedRecipe(updated);
      setPage(Page.DETAIL);
    }
  };

  useEffect(() => {
    if (selectedRecipe) {
      setTitle(selectedRecipe.title);
      setDescription(selectedRecipe.description);
      setIngredients(
        selectedRecipe.ingredients.map((obj) => ({ ...obj, createId: obj.id }))
      );
      setSteps(
        selectedRecipe.steps.map((obj) => ({ ...obj, createId: obj.id }))
      );
    }
  }, [selectedRecipe]);

  return (
    <div>
      <div className="flex justify-between items-center">
        <h1 className="font-bold text-3xl mb-4">
          {selectedRecipe ? "Update" : "Create"} Recipe
        </h1>
        <Button onClick={handleSubmit}>
          {selectedRecipe ? "Update" : "Create"}
        </Button>
      </div>
      <div className="space-y-8">
        <div className="flex flex-col space-y-1.5">
          <Label htmlFor="title">Title</Label>
          <Input
            id="title"
            placeholder="Title of your recipe"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </div>
        <div className="flex flex-col space-y-1.5">
          <Label htmlFor="desc">Description</Label>
          <Textarea
            id="desc"
            placeholder="Description of your recipe"
            rows={10}
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </div>
        <div className="grid grid-cols-2 gap-8">
          <div className="col-span-2 md:col-span-1">
            <div className="flex justify-between items-center mb-4">
              <h2 className="text-xl font-semibold ">Ingredients</h2>
              <Button size="icon" onClick={handleAddIngredientRow}>
                <Plus />
              </Button>
            </div>
            <div className="flex flex-col gap-4">
              {ingredients.map((st) => (
                <IngredientRow
                  key={st.createId}
                  ingredient={st}
                  setIngredients={setIngredients}
                />
              ))}
            </div>
          </div>
          <div className="col-span-2 md:col-span-1">
            <div className="flex justify-between items-center mb-4">
              <h2 className="text-xl font-semibold ">Steps</h2>
              <Button size="icon" onClick={handleAddStepRow}>
                <Plus />
              </Button>
            </div>
            <div className="flex flex-col gap-4">
              {steps.map((st) => (
                <StepRow key={st.createId} step={st} setSteps={setSteps} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
