import RecipeCard from "@/components/RecipeCard";
import { useRecipes } from "@/contexts/RecipeContext";
import { Page } from "@/types";
import { Plus } from "lucide-react";

export default function Home() {
  const { recipes, setPage, setSelectedRecipe, favoriteMode, favorites } =
    useRecipes();
  return (
    <div>
      <div className="grid grid-cols-12 gap-8">
        {recipes
          .filter((r) => !favoriteMode || favorites.includes(r.id))
          .map((recipe) => (
            <div
              className="col-span-3"
              key={recipe.id}
              onClick={() => {
                setSelectedRecipe(recipe);
                setPage(Page.DETAIL);
              }}
            >
              <RecipeCard recipe={recipe} />
            </div>
          ))}
      </div>
      <button
        onClick={() => {
          setSelectedRecipe(null);
          setPage(Page.FORM);
        }}
        className="fixed bg-green-700 hover:bg-green-600 text-white font-bold w-12 h-12 rounded-full right-8 bottom-8 flex items-center justify-center"
      >
        <Plus />
      </button>
    </div>
  );
}
