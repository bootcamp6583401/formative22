/* v8 ignore next 200 */

import { Button } from "@/components/ui/button";
import { useRecipes } from "@/contexts/RecipeContext";
import axios from "@/lib/axios";
import { Page, Recipe } from "@/types";
import { Pencil, Trash2Icon } from "lucide-react";

export default function Details({ recipe }: { recipe: Recipe }) {
  const { setPage, setSelectedRecipe, setRecipes } = useRecipes();
  return (
    <div>
      <h1 className="text-3xl font-bold mb-4">Recipe Details</h1>
      <div className="flex mb-8">
        <div className="w-1/2">
          <img
            className="w-full object-cover h-auto"
            src="https://d1vbn70lmn1nqe.cloudfront.net/prod/wp-content/uploads/2023/07/31040437/ini-resep-bumbu-rendang-daging-sapi-untuk-acara-spesial.jpg"
          />
        </div>
        <div className="w-1/2 px-4">
          <div className="flex justify-between">
            <h2 className="text-3xl font-bold mb-4">{recipe.title}</h2>
            <div className="flex gap-2">
              <Button
                className="bg-orange-600"
                size="icon"
                onClick={() => {
                  setSelectedRecipe(recipe);
                  setPage(Page.FORM);
                }}
              >
                <Pencil />
              </Button>
              <Button
                className=""
                variant="destructive"
                size="icon"
                onClick={async () => {
                  try {
                    await axios.delete(`/recipes/${recipe.id}`);
                    setRecipes((prev) =>
                      prev.filter((r) => r.id !== recipe.id)
                    );
                    setPage(Page.HOME);
                  } catch (error) {}
                }}
              >
                <Trash2Icon />
              </Button>
            </div>
          </div>
          <p className="tracking-light text-lg">{recipe.description}</p>
        </div>
      </div>
      <div className="grid grid-cols-2 gap-8">
        <div className="col-span-2 md:col-span-1">
          <h2 className="text-xl font-semibold mb-4">Ingredients</h2>
          <div className="flex flex-col gap-2">
            {recipe.ingredients.map((obj) => (
              <div
                key={obj.id}
                className="rounded-md bg-secondary text-secondary-foreground p-4 flex justify-between items-center"
              >
                <div>{obj.name}</div>
                <div>{obj.qty}</div>
              </div>
            ))}
          </div>
        </div>
        <div className="col-span-2 md:col-span-1">
          <h2 className="text-xl font-semibold mb-4">Steps</h2>
          <div className="flex flex-col gap-2">
            {recipe.steps.map((obj) => (
              <div
                key={obj.id}
                className="rounded-md bg-secondary text-secondary-foreground p-4"
              >
                {obj.name}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
